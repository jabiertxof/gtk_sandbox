#ifndef INK_MULTIPANED_H
#define INK_MULTIPANED_H

/*
 * A widget with multiple panes. Agnotic to type what kind of widgets panes contain.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <gtkmm.h>

/** A widget with multiple panes.

  Multipaned can contain:
    * No widget (first_widget == nullptr);
    * One widget (first_widget != Gtk::Paned widget)
    * One or more paned widgets (first widget == Gtk::Paned widget).

  For each Gtk::Paned widget:
    * Child 1 is a non-Gtk::Paned widget.
    * Child 2 is a non-Gtk::Paned widget or a Gtk::Paned widget.

  For example, to contain four panes (containing four widgets) one has:

     P
     '-> W  P
            '-> W  P
                   '-> W W

   where each line corresponds to a Gtk::Paned widget (W is any widget, P is a Paned widget),

 */


class InkMultipaned : public Gtk::Box
{
 public:
  InkMultipaned(Gtk::Orientation orientation=Gtk::ORIENTATION_HORIZONTAL);
  virtual ~InkMultipaned() {};

  void set_target_entries(const std::vector<Gtk::TargetEntry>& target_entries);
  void set_empty_widget (Gtk::Widget& widget);
  void prepend (Gtk::Widget& widget);
  void append  (Gtk::Widget& widget);
  void remove  (Gtk::Widget& widget);

  Gtk::Widget* get_first_widget() { return _first_widget; }
  Gtk::Widget* get_last_widget();

  void dump(Gtk::Widget* widget, int level = 0);
  void dump();

 private:
  Gtk::EventBox* prepend_eventbox;
  Gtk::EventBox* append_eventbox;
  Gtk::EventBox* prepare_eventbox(Gtk::Orientation orientation=Gtk::ORIENTATION_HORIZONTAL);

  Gtk::Widget* _first_widget;
  Gtk::Widget* get_last_widget(Gtk::Widget* widget);

  Gtk::Widget* _empty_widget;

  Gtk::Orientation _orientation;

  int get_drop_zone(int x, int y);

  // Callbacks  ('prepend' and 'append' refer to left/top most and right/bottom most buttons)
  bool on_drag_motion(const Glib::RefPtr<Gdk::DragContext> context, int x, int y, guint time, Gtk::EventBox *dropwidget);
  void on_drag_leave(const Glib::RefPtr<Gdk::DragContext> context, guint time, Gtk::EventBox *dropwidget);

  void on_drag_data(  const Glib::RefPtr<Gdk::DragContext> context, int x, int y,
                      const Gtk::SelectionData& selection_data, guint info, guint time);

  // TO DELETE
  void on_prepend_drag_data(const Glib::RefPtr<Gdk::DragContext> context, int x, int y,
                            const Gtk::SelectionData& selection_data, guint info, guint time);
  void on_append_drag_data(const Glib::RefPtr<Gdk::DragContext> context, int x, int y,
                           const Gtk::SelectionData& selection_data, guint info, guint time);
  
  // Signals
 public:
  sigc::signal<void, const Glib::RefPtr<Gdk::DragContext> > signal_prepend_drag_data();
  sigc::signal<void, const Glib::RefPtr<Gdk::DragContext> > signal_append_drag_data();
  sigc::signal<void> signal_now_empty();

 protected:
  sigc::signal<void, const Glib::RefPtr<Gdk::DragContext> > _signal_prepend_drag_data;
  sigc::signal<void, const Glib::RefPtr<Gdk::DragContext> > _signal_append_drag_data;
  sigc::signal<void> _signal_now_empty;
};

#endif // INK_MULTIPANED_H
