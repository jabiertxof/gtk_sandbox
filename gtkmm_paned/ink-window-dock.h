#ifndef INK_WINDOW_DOCK_H
#define INK_WINDOW_DOCK_H

/*
 * A window for floating docks. To do: make a base class of InkWIndow?
 */

#include <gtkmm.h>

class InkDialogContainer;
class InkMultipaned;

class InkWindowDock : public Gtk::ApplicationWindow
{
public:
  InkWindowDock(Gtk::Widget* page = nullptr);
  virtual ~InkWindowDock() {};

  void set_document(Glib::ustring doc) { document = doc; };
  Glib::ustring get_document() { return document; };

  void update_dialogs();
  InkDialogContainer* get_container() { return container; };

  void on_close();
  void on_new_dialog(Glib::ustring value);

private:
  Glib::RefPtr<Gtk::Builder> builder;

  InkDialogContainer* container;
  InkMultipaned* columns;

  Glib::ustring document;
};

#endif //INK_WINDOW_DOCK_H
