
#include "ink-window.h"
#include "ink-dialog-container.h"
#include "ink-multipaned.h"  // Move Drawing area to container code and remove this.
#include "ink-dialog-notebook.h"

#include <iostream>

InkWindow::InkWindow()
  : Gtk::ApplicationWindow()
{

  // ========================  Actions ==========================

  add_action_radio_string ("new_dialog", sigc::mem_fun(*this, &InkWindow::on_new_dialog), "Preferences");
  add_action ("close", sigc::mem_fun(*this, &InkWindow::close));
  add_action_radio_string ("change_tool", sigc::mem_fun(*this, &InkWindow::on_action_change_tool), "Selection");

  // ========================= Builder ==========================

  builder = Gtk::Builder::create();

  // Menu
  try
    {
      builder->add_from_file("ink-window.xml");
    }
  catch (const Glib::Error& error)
    {
      std::cerr << "InkWindow::InkWindow: Building window menu failed: " << error.what() << std::endl;
    }

  // Tools
  try
    {
      builder->add_from_file("tools.xml");
    }
  catch (const Glib::Error& error)
    {
      std::cerr << "InkWindow::InkWindow: tools error:" << error.what() << std::endl;
    }

  // Dialogs
  try
    {
      builder->add_from_file("dialogs.xml");
    }
  catch (const Glib::Error& error)
    {
      std::cerr << "InkWindow::InkWindow: dialog menu error:" << error.what() << std::endl;
    }


  // ========================= Widgets ==========================

  // ================ Window ==================
  static int i = 0;
  set_title("InkWindow " + Glib::ustring::format(++i));
  set_name("Document Window " + Glib::ustring::format(i));
  set_default_size(800, 400);


  // =============== Outer Box ================
  Gtk::Box* box_outer = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
  box_outer->set_name("Outer Box");
  add (*box_outer);


  // ============== Main Menu Bar =============
  Gtk::MenuBar* menu_bar = nullptr;
  builder->get_widget("menu-main", menu_bar);
  if (!menu_bar) {
    std::cerr << "InkWindow::InkWindow: Main menu bar not found!" << std::endl;
    // We should probably abort!
  } else {
    box_outer->pack_start(*menu_bar, Gtk::PACK_SHRINK);
  }

  Gtk::Menu* menu_dialogs = nullptr;
  builder->get_widget("menu-dialogs", menu_dialogs);
  if (!menu_dialogs) {
    std::cerr << "InkWindow::InkWindow: dialog menu not found!" << std::endl;
  } else {
    Gtk::MenuItem* menu_item = Gtk::manage(new Gtk::MenuItem("Dialogs"));
    menu_item->set_name("MainMenuBarDialogs");
    menu_bar->append(*menu_item);
    menu_item->set_submenu(*menu_dialogs);
    menu_bar->show_all();
  }


  // =============== Container ================
  container = Gtk::manage(new InkDialogContainer());
  box_outer->pack_start (*container);
  columns = container->get_columns();


  // ============= Initial Column =============
  InkMultipaned* column = container->create_column();
  columns->append(*column);


  // ============== Status Bar ===============
  
  // Why isn't this at bottom????
  Gtk::Label* status_bar = Gtk::manage(new Gtk::Label("Status Bar"));
  box_outer->pack_start (*status_bar, Gtk::PACK_SHRINK);


  // ============ Drawing Area ===============

  logo = Gtk::manage(new Gtk::Image("icons/inkscape_flat.svg"));
  logo->set_vexpand();
  logo->set_hexpand();
  on_change_background("lightgray");

  // image->set_size_request(400,400);
  columns->prepend(*logo);


  // ================ Toolbox ================

  Gtk::Box* box_flowbox = nullptr;
  builder->get_widget("tools", box_flowbox);
  if (!box_flowbox) {
    std::cerr << "InkWindow::InkWindow: tool box not found!" << std::endl;
  } else {
    columns->prepend(*box_flowbox);
  }

  // Save pointer to tool flowbox for use in callback.
  tools_flowbox = nullptr;
  builder->get_widget("tools-flowbox", tools_flowbox);
  if (!tools_flowbox) {
    std::cerr << "InkWindow::InkWindow: tool flowbox not found!" << std::endl;
  } else {
    Gtk::Widget* widget = tools_flowbox->get_children()[0];
    Gtk::FlowBoxChild* child = dynamic_cast<Gtk::FlowBoxChild*>(widget);
    if (child) {
      tools_flowbox->select_child(*child);
    }
  }
  auto provider = Gtk::CssProvider::create();
  try {
    provider->load_from_path("test-application.css");
  }
  catch (...) {
  }

  auto const screen = Gdk::Screen::get_default();
  Gtk::StyleContext::add_provider_for_screen (screen, provider, GTK_STYLE_PROVIDER_PRIORITY_USER);
}


// =====================  Callbacks ======================

// Can't override window->close();
void
InkWindow::on_close()
{
  std::cout << "InkWindow::on_close: " << get_title() << std::endl;
  // Check if saved.
  delete this;
}

// Adds new dialog to upper right of window.
void
InkWindow::on_new_dialog(Glib::ustring value)
{
  std::cout << "InkWindow::on_new_dialog: " << value << "!" << std::endl;
  container->new_dialog(value);
}

void
InkWindow::on_action_change_tool(Glib::ustring value)
{
  // std::cout << "Changed to tool: " << value << "!" << std::endl;

  // Select button in toolbox.
  // This is a rather nasty workaround for not having a Gtk::RadioButton mode that highlights
  // the selected icon without displaying the selection circle in front.
  Gtk::Button* button = nullptr;
  builder->get_widget(value, button);
  if (button) {
    Gtk::FlowBoxChild* flowboxchild = dynamic_cast<Gtk::FlowBoxChild*>(button->get_parent());
    if (flowboxchild) {
      tools_flowbox->select_child(*flowboxchild);
    }
  }
}

void
InkWindow::on_change_background(Glib::ustring value)
{
    Glib::RefPtr<Gtk::StyleContext> style = logo->get_style_context();

    Glib::RefPtr<Gtk::CssProvider> provider = Gtk::CssProvider::create();
    Glib::ustring css = "* {background-color:" + value + ";}";
    std::cout << "InkWindow::on_change_background: " << css << std::endl;
    provider->load_from_data(css);
    style->context_save();
    style->add_provider(provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    style->context_restore();
}
