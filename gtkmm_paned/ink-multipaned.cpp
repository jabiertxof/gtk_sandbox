
/*
 * A widget with multiple panes. Agnotic to type what kind of widgets panes contain.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "ink-multipaned.h"
#include "ink-dialog-notebook.h"  // Only for debugging, to remove.

#include <iostream>

InkMultipaned::InkMultipaned (Gtk::Orientation orientation)
  : Gtk::Box (orientation),
    _orientation(orientation),
    _first_widget(nullptr),
    _empty_widget(nullptr)
{
  set_name("InkMultipaned");

  set_vexpand();
  set_hexpand();

  // signal_drag_motion().connect(
  //    sigc::mem_fun(*this, &InkMultipaned::on_drag_motion));
  signal_drag_data_received().connect(
     sigc::mem_fun(*this, &InkMultipaned::on_drag_data));

  prepend_eventbox = prepare_eventbox(orientation);
  append_eventbox  = prepare_eventbox(orientation);
  //this extra events are neceasry as says here https://developer.gnome.org/gtkmm-tutorial/stable/sec-dnd-signals.html.en
  //But the problem droping is in the new drag_dest_set options and in set v/hexpand
  prepend_eventbox->signal_drag_data_received().connect(
     sigc::mem_fun(*this, &InkMultipaned::on_prepend_drag_data));
  append_eventbox->signal_drag_data_received().connect(
     sigc::mem_fun(*this, &InkMultipaned::on_append_drag_data));
  prepend_eventbox->signal_drag_motion().connect(
     sigc::bind<Gtk::EventBox *>(sigc::mem_fun(*this, &InkMultipaned::on_drag_motion), prepend_eventbox));
  append_eventbox->signal_drag_motion().connect(
     sigc::bind<Gtk::EventBox *>(sigc::mem_fun(*this, &InkMultipaned::on_drag_motion), append_eventbox));
  prepend_eventbox->signal_drag_leave().connect(
     sigc::bind<Gtk::EventBox *>(sigc::mem_fun(*this, &InkMultipaned::on_drag_leave), prepend_eventbox));
  append_eventbox->signal_drag_leave().connect(
     sigc::bind<Gtk::EventBox *>(sigc::mem_fun(*this, &InkMultipaned::on_drag_leave), append_eventbox));

  auto label = Gtk::manage(new Gtk::Label("You can drop dockable dialogs here."));
  label->set_line_wrap();
  label->set_justify(Gtk::JUSTIFY_CENTER);
  label->set_valign(Gtk::ALIGN_CENTER);
  label->set_vexpand();

  _empty_widget = label;

  add (*prepend_eventbox);
  add (*_empty_widget);
  add (*append_eventbox);
}

void
InkMultipaned::set_target_entries(const std::vector<Gtk::TargetEntry>& target_entries)
{
  drag_dest_set (target_entries);
  prepend_eventbox->drag_dest_set (target_entries, Gtk::DEST_DEFAULT_MOTION | Gtk::DEST_DEFAULT_DROP, Gdk::ACTION_COPY | Gdk::ACTION_MOVE);
  append_eventbox->drag_dest_set (target_entries, Gtk::DEST_DEFAULT_MOTION | Gtk::DEST_DEFAULT_DROP, Gdk::ACTION_COPY | Gdk::ACTION_MOVE);
}

void
InkMultipaned::set_empty_widget(Gtk::Widget& widget)
{
  delete _empty_widget;
  _empty_widget = &widget;
}

// Inserts a widget on the left or top side of multipaned.
void
InkMultipaned::prepend (Gtk::Widget& widget)
{
  if (&widget == nullptr) {
    std::cerr << "InkMultipaned::prepend: widget is nullptr!" << std::endl;
    return;
  }

  if (_first_widget == nullptr) {

    // Directly add new widget.
    Gtk::Box::remove (*_empty_widget);
    add (widget);
    reorder_child (widget, 1);
    _first_widget = &widget;

  } else {

    // Create new Gtk::Paned
    Gtk::Paned* new_paned = Gtk::manage(new Gtk::Paned(_orientation));
    new_paned->set_wide_handle();
    new_paned->add1(widget);

    // Insert new Gtk::Paned at beginning.
    _first_widget->reparent(*new_paned);
    add(*new_paned);

    // Update first widget.
    _first_widget = new_paned;

    // Need to move Gtk::Paned to between prepend/append widgets.
    reorder_child(*new_paned, 1);
  }

  show_all_children();
  // dump();
}

// Inserts a widget on the right or bottom side of multipaned.
void
InkMultipaned::append (Gtk::Widget& widget)
{
  if (&widget == nullptr) {
    std::cerr << "InkMultipaned::append: widget is nullptr!" << std::endl;
    return;
  }

  if (_first_widget == nullptr) {

    // Directly add new widget.
    Gtk::Box::remove (*_empty_widget);
    add (widget);
    reorder_child (widget, 1);
    _first_widget = &widget;

  } else {

    // Create new Gtk::Paned
    Gtk::Paned* new_paned = Gtk::manage(new Gtk::Paned(_orientation));
    new_paned->set_wide_handle();
    new_paned->add2(widget);

    // Find old parent before we reparent last widget.
    Gtk::Widget* last_widget = get_last_widget();
    Gtk::Paned* old_paned = dynamic_cast<Gtk::Paned*>(last_widget->get_parent());

    // Insert new Gtk::Paned at end.
    last_widget->reparent(*new_paned);  // Add as child1.

    if (old_paned) {
      old_paned->add2(*new_paned);
    } else {
      add(*new_paned); // Adding to Gtk::Box
      reorder_child(*new_paned, 1); // Need to move pane to between prepend/append widgets.
      _first_widget = new_paned;
    }
  }

  show_all_children();
  // dump();
}

// Remove pane.
void
InkMultipaned::remove(Gtk::Widget& widget)
{
  // dump();

  // Only widget (no Gtk::Paned)
  Gtk::Box* box = dynamic_cast<Gtk::Box*>(widget.get_parent());
  if (box) {
    // std::cout << "InkMultipaned::remove: Box is parent" << std::endl;
    box->remove(widget);
    _first_widget = nullptr;

    // We are now an empty InkMultipaned.

    // Add place holder widget
    box->add (*_empty_widget);
    reorder_child (*_empty_widget, 1);
    box->show_all();

    _signal_now_empty.emit();

    return;
  }

  // Parent is Gtk::Paned, remove.
  Gtk::Paned* parent = dynamic_cast<Gtk::Paned*>(widget.get_parent());
  if (parent) {
    // std::cout << "InkMultipaned::remove: Multipaned is parent" << std::endl;

    Gtk::Container* grandparent = dynamic_cast<Gtk::Container*>(parent->get_parent());
    if (grandparent) {

      Gtk::Widget* to_move = nullptr;
      if (parent->get_child1() == &widget) {
        to_move = parent->get_child2();
      } else {
        to_move = parent->get_child1();
      }

      parent->remove(*to_move);
      grandparent->remove(*parent); // Remove extra Gtk::paned 
      delete parent; // Remove doesn't delete, empty widget deleted also
      grandparent->add(*to_move);

      // Check to see if grandparent is box (we've removed first paned).
      Gtk::Box* box = dynamic_cast<Gtk::Box*>(grandparent);
      if (box) {
        box->reorder_child(*to_move, 1);
        _first_widget = to_move;
      }

    } else {
      std::cerr << "InkMultipaned::remove: Missing Grandparent!" << std::endl;
    }

  } else {
    std::cerr << "InkMultipaned::remove: Parent is not Gtk::Paned!" << std::endl;
  }

  show_all_children();
  // dump();
}

void
InkMultipaned::dump(Gtk::Widget* widget, int level)
{
  ++level;

  if (_first_widget == nullptr) {
    std::cout << "  " << level << " Empty InkMultipaned" << std::endl;
  } else {
    Gtk::Paned* paned = dynamic_cast<Gtk::Paned*>(widget);
    if(paned) {
      std::cout << "  " << level << " Got Gtk::Paned: " << widget->get_name() << std::endl;
      dump (paned->get_child1(), level);
      dump (paned->get_child2(), level);
    } else {
      // To be removed...
      InkDialogNotebook* notebook = dynamic_cast<InkDialogNotebook*>(widget);
      if (notebook) {
        Gtk::Notebook* nb = notebook->get_notebook();
        std::string label;
        if (nb->get_n_pages() > 0) {
          label = nb->get_tab_label_text(*(nb->get_nth_page(0)));
        }
        std::cout << "  " << level << " Got InkDialogNotebook: " << label << std::endl;
      } else {
        std::cout << "  " << level << " Got something else! " << widget->get_name() << std::endl;
      }
    }
  }
}

void
InkMultipaned::dump()
{
  std::cout << "InkMultipaned::dump()" << std::endl;
  dump (_first_widget);
}

Gtk::EventBox*
InkMultipaned::prepare_eventbox(Gtk::Orientation orientation)
{
  Gtk::EventBox* eventbox = Gtk::manage(new Gtk::EventBox());

  eventbox->set_name("InkMultipanedDropBox");
  if (orientation == Gtk::ORIENTATION_HORIZONTAL) {
    eventbox->set_size_request(6, -1);
    eventbox->set_vexpand(true);
  } else {
    eventbox->set_size_request(-1, 6);
    eventbox->set_hexpand(true);
  }    

  eventbox->set_visible_window(true);
  eventbox->set_above_child(true); // Actually, no child.
  Gtk::Image *dropimage = dynamic_cast<Gtk::Image *>(eventbox->get_child());
  if (!dropimage) {
      dropimage = new Gtk::Image();
      dropimage->set("icons/on-symbolic.svg");
      dropimage->set_size_request (30, 30);
      dropimage->set_no_show_all();
      eventbox->add(*dropimage);
  }
  return eventbox;
}

Gtk::Widget*
InkMultipaned::get_last_widget(Gtk::Widget* widget)
{
  Gtk::Paned* paned = dynamic_cast<Gtk::Paned*>(widget);
  if ( paned ) {
    return get_last_widget(paned->get_child2());
  }
  return widget;
}

Gtk::Widget*
InkMultipaned::get_last_widget()
{
  return (get_last_widget( _first_widget));
}

// Returns:  NOT USED
//   0: Not in zone.
//   1: In prepend zone.
//   2: In append zone;
int
InkMultipaned::get_drop_zone(int x, int y)
{
  static int DROP_THICKNESS = 36;

  int zone = 0;
  if (_orientation == Gtk::ORIENTATION_HORIZONTAL) {
    if (x < DROP_THICKNESS) {
      zone = 1;
    } else if (x > (get_allocated_width() - DROP_THICKNESS)) {
      zone = 2;
    }
  } else {
    if (y < DROP_THICKNESS) {
      zone = 1;
    } else if (y > (get_allocated_height() - DROP_THICKNESS)) {
      zone = 2;
    }
  }

  // If empty pane, then all is drop zone.
  if (zone == 0 && _first_widget == nullptr) {
    zone = 1;
  }

  std::cout << "InkMultipaned::get_drag_zone: " << zone << std::endl;
  return zone;
}

// Callbacks

// Returns if cursor is in drop zone. NOT USED
bool
InkMultipaned::on_drag_motion(const Glib::RefPtr<Gdk::DragContext> context, int x, int y, guint time, Gtk::EventBox *dropwidget)
{

  std::cout << "InkMultipaned::on_drag_motion: x:" << x << " y: " << y << std::endl;

  int zone = get_drop_zone(x, y);
  if (zone == 0) {
    context->drag_refuse(time);
  }
  Gtk::Image *dropimage = dynamic_cast<Gtk::Image *>(dropwidget->get_child());
  if (dropimage) {
      dropimage->show();
  }
  dropwidget->get_style_context()->add_class("dropover");
  return (zone > 0);
}

void
InkMultipaned::on_drag_leave(const Glib::RefPtr<Gdk::DragContext> context, guint time, Gtk::EventBox *dropwidget)
{

  std::cout << "InkMultipaned::on_drag_leave: time:" << time << std::endl;
  Gtk::Image *dropimage = dynamic_cast<Gtk::Image *>(dropwidget->get_child());
  if (dropimage) {
      dropimage->hide();
  }
  dropwidget->get_style_context()->remove_class("dropover");
}

void
InkMultipaned::on_drag_data(const Glib::RefPtr<Gdk::DragContext> context, int x, int y,
                            const Gtk::SelectionData& selection_data, guint info, guint time)
{
  // std::cout << "InkMultipaned::on_drag_data" << std::endl;
  // int zone = get_drop_zone(x, y);

  // if (zone == 1) {
  //   _signal_prepend_drag_data.emit(context);
  // } else if (zone == 2) {
  //   _signal_append_drag_data.emit(context);
  // } else {
  //   std::cerr << "InkMultipaned::on_drag_data: Invalid zone!" << std::endl;
  // }
  _signal_prepend_drag_data.emit(context);
}

void
InkMultipaned::on_prepend_drag_data(const Glib::RefPtr<Gdk::DragContext> context, int x, int y,
                                    const Gtk::SelectionData& selection_data, guint info, guint time)
{
  std::cerr << "InkMultipaned::on_prepend_drag_data: DEPRICATED" << std::endl;
  _signal_prepend_drag_data.emit(context);
}

void
InkMultipaned::on_append_drag_data(const Glib::RefPtr<Gdk::DragContext> context, int x, int y,
                                   const Gtk::SelectionData& selection_data, guint info, guint time)
{
  std::cerr << "InkMultipaned::on_append_drag_data: DEPRICATED" << std::endl;
  _signal_append_drag_data.emit(context);
}

// Signals

sigc::signal<void, const Glib::RefPtr<Gdk::DragContext> >
InkMultipaned::signal_prepend_drag_data()
{
  return _signal_prepend_drag_data;
}

sigc::signal<void, const Glib::RefPtr<Gdk::DragContext> >
InkMultipaned::signal_append_drag_data()
{
  return _signal_append_drag_data;
}

sigc::signal<void>
InkMultipaned::signal_now_empty()
{
  return _signal_now_empty;
}
