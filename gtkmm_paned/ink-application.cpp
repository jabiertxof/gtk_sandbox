
/*
 * The main Inkscape application.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "ink-application.h"
#include "ink-window.h"
#include "ink-window-dock.h"

#include <iostream>

InkApplication::InkApplication()
  : Gtk::Application("org.inkscape.application")
{
  Glib::set_application_name("Inkscape - A Vector Drawing Program");
}

Glib::RefPtr<InkApplication> InkApplication::create()
{
  return Glib::RefPtr<InkApplication>(new InkApplication());
}

void
InkApplication::on_startup()
{
  Gtk::Application::on_startup();

  // ========================= Actions ==========================
  add_action("new",    sigc::mem_fun(*this, &InkApplication::on_new   ));
  add_action("quit",   sigc::mem_fun(*this, &InkApplication::on_quit  ));
  add_action("about",  sigc::mem_fun(*this, &InkApplication::on_about ));
  add_action_radio_string ("change_background", sigc::mem_fun(*this, &InkApplication::on_change_background), "gray");

  // ========================= Builder ==========================
  _builder = Gtk::Builder::create();

  try
    {
    _builder->add_from_file("ink-application.xml");
    }
  catch (const Glib::Error& ex)
    {
      std::cerr << "InkApplication: ink_application.xml file not read! " << ex.what() << std::endl;
    }

  auto object = _builder->get_object("menu-application");
  auto menu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);
  if (!menu) {
    std::cerr << "InkApplication: failed to load application menu!" << std::endl;
  } else {
    set_app_menu(menu);
  }
}

void
InkApplication::on_activate()
{
  std::cout << "InkApplication::on_activate" << std::endl;
  create_window();
}

void
InkApplication::create_window()
{
  std::cout << "InkApplication::create_window" << std::endl;
  auto window = new InkWindow();
  add_window(*window);

  window->signal_focus_in_event().connect(sigc::mem_fun(*this, &InkApplication::on_focus_in_event));
  window->show_all();
}

// Always created with one page from a notebook.
void
InkApplication::create_window_dock(Gtk::Widget* page)
{
  std::cout << "InkApplication::create_window_dock" << std::endl;
  auto window = new InkWindowDock(page);
  add_window(*window);

  // We don't need to respond to focus in events.
  window->show_all();
}

//   ========================  Actions  =========================

void
InkApplication::on_new()
{
  std::cout << "InkApplication::on_new" << std::endl;

  create_window();
}

void
InkApplication::on_quit()
{
  // Delete all windows (quit() doesn't do this).
  std::vector<Gtk::Window*> windows = get_windows();
  for (auto window: windows) {
    InkWindow* inkwindow = dynamic_cast<InkWindow*>(window);
    if (inkwindow) {
      inkwindow->on_close();
    }
    InkWindowDock* inkwindowdock = dynamic_cast<InkWindowDock*>(window);
    if (inkwindowdock) {
      inkwindowdock->on_close();
    }
  }

  quit();
}

void
InkApplication::on_about()
{
  std::cout << "InkApplication::on_about" << std::endl;
}

void
InkApplication::on_change_background(Glib::ustring value)
{
  // Find first document window which must contain "active" desktop.
  std::vector<Gtk::Window*> windows = get_windows();
  for (auto window : windows) {
    InkWindow* document = dynamic_cast<InkWindow*>(window);
    if (document) {
      document->on_change_background(value);
      return;
    }
  }
}

//   ======================== Callbacks =========================

bool
InkApplication::on_focus_in_event(GdkEventFocus* gdk_event)
{
  std::vector<Gtk::Window*> windows = get_windows();

  Glib::ustring document_name;

  // Find name of last in-focus document window.
  for (auto window : windows) {
    InkWindow* document = dynamic_cast<InkWindow*>(window);
    if (document) {
      document_name = document->get_name();
      break;
    }
  }

  // Set floating dock document
  for (auto window : windows) {
    InkWindowDock* dock = dynamic_cast<InkWindowDock*>(window);
    if (dock) {
      dock->set_document(document_name);
      dock->update_dialogs();
    }
  }

  return true;
}
