
/*
 * A widget that manages InkNotebook's and other widgets inside a horizontal InkMultipane
 * containing vertical InkMultipane's or other widgets.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "ink-dialog-container.h"
#include "ink-dialog-notebook.h"
#include "ink-dialog-base.h"
#include "ink-dialog-palette.h"
#include "ink-multipaned.h"

#include <iostream>

InkDialogContainer::InkDialogContainer()
{
  set_name("InkDialogContainer");

  // Setup Column InkMultipane
  columns = Gtk::manage(new InkMultipaned(Gtk::ORIENTATION_HORIZONTAL));

  columns->signal_prepend_drag_data().connect(sigc::bind<InkMultipaned*>(
     sigc::mem_fun(*this, &InkDialogContainer::prepend_drop), columns));

  columns->signal_append_drag_data().connect(sigc::bind<InkMultipaned*>(
     sigc::mem_fun(*this, &InkDialogContainer::append_drop), columns));

  // Setup drop targets.
  target_entries.push_back( Gtk::TargetEntry("GTK_NOTEBOOK_TAB")) ;
  //target_entries.push_back( Gtk::TargetEntry("application/x-rootwindow-drop")) ;
  columns->set_target_entries(target_entries);

  add (*columns);

  // Should probably be moved to window.
  signal_unmap().connect(sigc::mem_fun(*this, &InkDialogContainer::on_unmap));

  show_all_children();
}

InkMultipaned*
InkDialogContainer::get_columns()
{
  return columns;
}

InkMultipaned*
InkDialogContainer::create_column()
{
  InkMultipaned* column = Gtk::manage(new InkMultipaned(Gtk::ORIENTATION_VERTICAL));

  column->signal_prepend_drag_data().connect(sigc::bind<InkMultipaned*>(
     sigc::mem_fun(*this, &InkDialogContainer::prepend_drop), column));

  column->signal_append_drag_data().connect(sigc::bind<InkMultipaned*>(
     sigc::mem_fun(*this, &InkDialogContainer::append_drop), column));

  column->signal_now_empty().connect(sigc::bind<InkMultipaned*>(
     sigc::mem_fun(*this, &InkDialogContainer::column_empty), column));

  column->set_target_entries(target_entries);

  return column;
}

std::pair<InkDialogBase*, Gtk::Widget*>
InkDialogContainer::create_dialog_widgets(Glib::ustring value)
{
  InkDialogBase* dialog = nullptr;
  if (value == "Palette") {
    dialog = Gtk::manage(new InkDialogPalette(value));
  } else {
    dialog = Gtk::manage(new InkDialogBase(value));
  }
  dialog->init();

  // Create tab
  Gtk::Box* tab = Gtk::manage(new Gtk::Box());
  tab->set_name("InkDialogTab");
  Gtk::Label* label = Gtk::manage(new Gtk::Label(value));
  Gtk::Image* image = Gtk::manage(new Gtk::Image());
  if (value == "Export") {
    image->set_from_icon_name("document-save-as", Gtk::ICON_SIZE_MENU);
  } else if (value == "Document Properties") {
    image->set_from_icon_name("document-properties", Gtk::ICON_SIZE_MENU);
  } else if (value == "Undo History") {
    image->set_from_icon_name("edit-undo", Gtk::ICON_SIZE_MENU);
  } else if (value == "Find") {
    image->set_from_icon_name("edit-find", Gtk::ICON_SIZE_MENU);
  } else if (value == "Palette") {
    image->set_from_icon_name("color-select", Gtk::ICON_SIZE_MENU);
  } else if (value == "Export") {
    image->set_from_icon_name("unknown", Gtk::ICON_SIZE_MENU);
  }

  tab->pack_start(*image);
  tab->pack_end(*label);
  tab->show_all();
 
  return std::make_pair(dialog, tab);
}

// Add new dialog (in response to menu)
void
InkDialogContainer::new_dialog(Glib::ustring value)
{
  // Limit each container to containing one of any type of dialog.
  auto it = dialogs.find(value);
  if ( it != dialogs.end()) {
    std::cerr << "InkDialogContainer::new_dialog: Already has a \"" << value
              << "\" dialog!" << std::endl;

    // Blink notebook with existing dialog to let user know where it is and show page.
    it->second->blink();
    return;
  }
  
  // Get page and tab to insert.
  std::pair<InkDialogBase*, Gtk::Widget*> widgets = create_dialog_widgets(value);
  InkDialogBase* dialog = widgets.first;
  Gtk::Widget* tab = widgets.second;

  // Find or create notebook to insert dialog
  InkDialogNotebook* notebook = nullptr;

  // Check if request came from notebook menu
  Gtk::Window* window = dynamic_cast<Gtk::Window*>(get_toplevel());
  if (window) {
    Gtk::Widget* focus = window->get_focus();
    if (focus) {
      Gtk::Notebook* gtknotebook =
        dynamic_cast<Gtk::Notebook*>(focus->get_ancestor(GTK_TYPE_NOTEBOOK));
      if (gtknotebook) {
        notebook = dynamic_cast<InkDialogNotebook*>(gtknotebook->get_parent());
      }
    }
  }

  // If not from notebook menu add at top of last column.
  if (!notebook) {
    // Look to see if last column contains a multipane. If not, add one.
    InkMultipaned* last_column = dynamic_cast<InkMultipaned*>(columns->get_last_widget());
    if (!last_column) {
      last_column = create_column();
      columns->append(*last_column);
    }

    // Look to see if first widget in column is notebook, if not add one.
    notebook = dynamic_cast<InkDialogNotebook*>(last_column->get_first_widget());
    if (!notebook) {
      notebook = Gtk::manage(new InkDialogNotebook(this));
      last_column->prepend(*notebook);
    }
  }

  // Add "dialog"
  notebook->add_page(*dialog, *tab, value);

  list_dialogs();
}


// Update dialogs:
void
InkDialogContainer::update_dialogs()
{
  for (auto dialog : dialogs) {
    dialog.second->update();
  }
}


// For debugging.
void
InkDialogContainer::list_dialogs()
{
  std::cout << "InkDialogContainer::list_dialogs:" << std::endl; 
  for (auto dialog : dialogs) {
    std::cout << "    Dialog: " << dialog.first << std::endl;
  }
}

bool
InkDialogContainer::has_dialog_of_type(InkDialogBase* dialog)
{
  return (dialogs.find(dialog->get_name()) != dialogs.end());
}

// Add dialog to list.
void
InkDialogContainer::link_dialog(InkDialogBase* dialog)
{
  dialogs.insert(std::pair<Glib::ustring, InkDialogBase*>(dialog->get_name(), dialog));
}

// Remove dialog from list.
void
InkDialogContainer::unlink_dialog(InkDialogBase* dialog)
{
  // We can't use find here as we want to remove a specific dialog
  // and find can return any dialog with the same type.
  for (auto it = dialogs.begin(); it != dialogs.end(); ++it) {
    if (it->second == dialog) {
      dialogs.erase(it);
      break;
    }
  }
}


// Signals -----------------------------------------------------

// No zombie windows. Need to work on this as it still leaves Gtk::Window!
// And segfaults if window closed with dialog still inside.
void
InkDialogContainer::on_unmap()
{
  std::cout << "InkDialogContainer::on_unmap(): Entrance" << std::endl;
  // Need to clean up signals.
  // delete this;
}


// Create a new notebook and move page.
InkDialogNotebook*
InkDialogContainer::prepare_drop(const Glib::RefPtr<Gdk::DragContext> context)
{
  Gtk::Widget* source = Gtk::Widget::drag_get_source_widget(context);
  // Find source notebook and page
  Gtk::Notebook* old_notebook = dynamic_cast<Gtk::Notebook*>(source);
  if (!old_notebook) {
    std::cerr << "InkDialogContainer::prepare_drop: notebook not found!" << std::endl;
    return nullptr;
  }

  // Find page
  Gtk::Widget* page = old_notebook->get_nth_page( old_notebook->get_current_page() );
  if (!page) {
    std::cerr << "InkDialogContainer::prepare_drop: page not found!" << std::endl;
    return nullptr;
  }

  // Create new notebook and move page.
  InkDialogNotebook* new_notebook = Gtk::manage(new InkDialogNotebook(this));
  new_notebook->move_page(*page);

  // move_page() takes care of updating dialog lists.
  list_dialogs();

  return new_notebook;
}


// Notebook page dropped on prepend target. Call function to create new notebook and then insert.
void
InkDialogContainer::prepend_drop(const Glib::RefPtr<Gdk::DragContext> context, InkMultipaned* multipane)
{
  InkDialogNotebook* new_notebook = prepare_drop(context); // Creates notebook, moves page.
  if (!new_notebook) {
    std::cerr << "InkDialogContainer::prepend_drop: no new notebook!" << std::endl;
    context->drag_finish(false, false, 0);
    return;
  }

  if (multipane->get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
    // Columns
    // Create column
    InkMultipaned* column = create_column();
    column->prepend (*new_notebook);
    columns->prepend (*column);
  } else {
    // Column
    multipane->prepend(*new_notebook);
  }
  context->drag_finish(true, false, 0);
}


// Notebook page dropped on append target. Call function to create new notebook and then insert.
void
InkDialogContainer::append_drop(const Glib::RefPtr<Gdk::DragContext> context, InkMultipaned* multipane)
{
  InkDialogNotebook* new_notebook = prepare_drop(context); // Creates notebook, moves page.
  if (!new_notebook) {
    std::cerr << "InkDialogContainer::append_drop: no new notebook!" << std::endl;
    context->drag_finish(false, false, 0);
    return;
  }

  if (multipane->get_orientation() == Gtk::ORIENTATION_HORIZONTAL) {
    // Columns
    // Create column
    InkMultipaned* column = create_column();
    column->prepend (*new_notebook);
    columns->append (*column);
  } else {
    // Column
    multipane->append(*new_notebook);
  }
  context->drag_finish(true, false, 0);
}


// Delete column if empty but not if it's the only one containing a multipane at end.
void
InkDialogContainer::column_empty(InkMultipaned* column)
{
  // We want to keep the column alive if it is the only InkMultipaned at the end.
  // This always gives the user one column to drop dialogs into.

  // If it is not the last column, delete it.
  if (column != columns->get_last_widget()) {
    columns->remove(*column);
    return;
  }

  // OK, it's the last column. Let's see if the second to last is an InkMultipaned.
  Gtk::Widget* widget = column->get_parent();
  Gtk::Paned* parent = dynamic_cast<Gtk::Paned*>(widget);
  if (!parent) {
    // parent must be Gtk::Paned if there is a second to last column.
    return;
  }

  widget = parent->get_child1();
  InkMultipaned* second_to_last = dynamic_cast<InkMultipaned*>(widget);
  if (second_to_last) {
    columns->remove(*column);
  }
}
