
#include "ink-window-dock.h"
#include "ink-dialog-container.h"
#include "ink-multipaned.h"  // Move Drawing area to container code and remove this.
#include "ink-dialog-notebook.h"

#include <iostream>

// Create dock and move page from old notebook.
InkWindowDock::InkWindowDock(Gtk::Widget* page)
  : Gtk::ApplicationWindow()
{

  // ========================  Actions ==========================

  add_action_radio_string ("new_dialog", sigc::mem_fun(*this, &InkWindowDock::on_new_dialog), "Preferences");
  add_action ("close", sigc::mem_fun(*this, &InkWindowDock::close));

  // ========================= Widgets ==========================

  // ================ Window ==================
  static int i = 0;
  set_title("InkWindowDock " + Glib::ustring::format(++i));
  set_name("Dock Window " + Glib::ustring::format(i));
  set_default_size(300, 400);


  // =============== Outer Box ================
  Gtk::Box* box_outer = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
  box_outer->set_name("Outer Box");
  add (*box_outer);


  // =============== Container ================
  container = Gtk::manage(new InkDialogContainer());
  box_outer->pack_end (*container);
  columns = container->get_columns();


  // ============= Initial Column =============
  InkMultipaned* column = container->create_column();
  columns->append(*column);


  // ============== New Notebook ==============
  InkDialogNotebook* ink_notebook = Gtk::manage(new InkDialogNotebook(container));
  column->append(*ink_notebook);
  ink_notebook->move_page(*page);
}

void
InkWindowDock::update_dialogs()
{
  container->update_dialogs();
}

// =====================  Callbacks ======================

// Can't override window->close();
void
InkWindowDock::on_close()
{
  std::cout << "InkWindowDock::on_close: " << get_title() << std::endl;
  // Check if saved.
  delete this;
}

// Adds new dialog to upper right of window.
void
InkWindowDock::on_new_dialog(Glib::ustring value)
{
  std::cout << "InkWindowDock::on_new_dialog: " << value << "!" << std::endl;
  container->new_dialog(value);
}
