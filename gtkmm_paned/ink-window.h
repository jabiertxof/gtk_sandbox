#ifndef INK_WINDOW_H
#define INK_WINDOW_H

#include <gtkmm.h>

class InkDialogContainer;
class InkMultipaned;

class InkWindow : public Gtk::ApplicationWindow
{
public:
  InkWindow();
  virtual ~InkWindow() {};

  void on_close();
  void on_new_dialog(Glib::ustring value);
  void on_action_change_tool(Glib::ustring value);
  void on_change_background(Glib::ustring value);

private:
  Glib::RefPtr<Gtk::Builder> builder;

  InkDialogContainer* container;
  InkMultipaned* columns;

  Gtk::FlowBox* tools_flowbox;
  Gtk::Image* logo;
};

#endif //INK_WINDOW_H
