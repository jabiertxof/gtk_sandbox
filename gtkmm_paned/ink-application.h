#ifndef INK_APPLICATION_H
#define INK_APPLICATION_H

/*
 * The main Inkscape application.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <gtkmm.h>

class InkDialogBase;

class InkApplication : public Gtk::Application
{
 protected:
  InkApplication();

 public:
  static Glib::RefPtr<InkApplication> create();

 protected:
  void on_startup() override;
  void on_activate() override;

 private:
  void create_window();

 public:
  void create_window_dock(Gtk::Widget* page);

 private:
  // Callbacks
  void on_new();
  void on_quit();
  void on_about();
  void on_change_background(Glib::ustring value);
  bool on_focus_in_event(GdkEventFocus* gdk_event);

  Glib::RefPtr<Gtk::Builder> _builder;
};

#endif // INK_APPLICATION_H
