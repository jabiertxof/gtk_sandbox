
/*
 * A base class for all dialogs.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "ink-dialog-base.h"
#include "ink-window-dock.h"

#include <iostream>

InkDialogBase::InkDialogBase(Glib::ustring name)
  : Gtk::Box()
  , _name(name)
{
  std::cout << "InkDialogBase::InkDialogBase: " << name << std::endl;
  set_name(name);
}

void
InkDialogBase::init()
{
  // Set label, we do this ourselves so we can set line wrap.
  Gtk::Label* label = Gtk::manage(new Gtk::Label(_name));
  label->set_line_wrap();

  _debug_button.set_name("InkDialogDebugButton");
  _debug_button.set_hexpand();
  _debug_button.signal_clicked().connect(sigc::mem_fun(*this, &InkDialogBase::on_click));

  _debug_button.add(*label);
  add(_debug_button);
}

// Highlight notebook where dialog already exists.
void
InkDialogBase::blink()
{
  Gtk::Notebook* notebook = dynamic_cast<Gtk::Notebook*>(get_parent());
  if (notebook && notebook->get_is_drawable()) {

    // Switch notebook to this dialog.
    int page_number = notebook->page_num(*this);
    notebook->set_current_page(page_number);

    Glib::RefPtr<Gtk::StyleContext> style = notebook->get_style_context();

    Glib::RefPtr<Gtk::CssProvider> provider = Gtk::CssProvider::create();
    provider->load_from_data(" *.blink {padding:2px;background-color:green;}");
    style->add_provider(provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    style->add_class("blink");

    // Add timer to turn off blink.
    sigc::slot<bool> slot = sigc::mem_fun(*this, &InkDialogBase::blink_off);
    sigc::connection connection = Glib::signal_timeout().connect(slot, 500); // msec
  }
}

void
InkDialogBase::update()
{
  Gtk::Window* window = dynamic_cast<Gtk::Window*>(get_toplevel());
  if (!window) {
    std::cerr << "InkDialogBase::update(): Failed to get window!" << std::endl;
    return;
  }

  InkWindowDock* dock = dynamic_cast<InkWindowDock*>(window);
  Gtk::Label* label = dynamic_cast<Gtk::Label*>(_debug_button.get_child());
  if (dock && label) {
    label->set_text(get_name() + " " + dock->get_document());
  }
}

bool
InkDialogBase::blink_off()
{
  Gtk::Notebook* notebook = dynamic_cast<Gtk::Notebook*>(get_parent());
  if (notebook && notebook->get_is_drawable()) {
    Glib::RefPtr<Gtk::StyleContext> style = notebook->get_style_context();
    style->remove_class("blink");
  }
  return false;
}

void
InkDialogBase::on_click()
{
  std::cout << "InkDialogBase::on_click: " << _name << std::endl;
  // Warning: get_window() returns Gdk::Window, not Gtk::Window (which is a totally different beast).

  Gtk::Window* window = dynamic_cast<Gtk::Window*>(get_toplevel());
  if (window) {
    std::cout << "Dialog is part of: " << window->get_name()
              << "  (" << window->get_title() << ")" << std::endl;
  } else {
    std::cerr << "InkDialogBase::on_click(): Dialog not attached to window!" << std::endl;
  }
}
