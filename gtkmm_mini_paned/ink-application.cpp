
/*
 * The main Inkscape application.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include "ink-application.h"
#include <iostream>

InkApplication::InkApplication()
  : Gtk::Application("org.inkscape.application")
{
  Glib::set_application_name("Inkscape - A Vector Drawing Program");
}

Glib::RefPtr<InkApplication> InkApplication::create()
{
  return Glib::RefPtr<InkApplication>(new InkApplication());
}

void
InkApplication::on_startup()
{
  Gtk::Application::on_startup();


}
void
InkApplication::on_activate()
{
  std::cout << "InkApplication::on_activate" << std::endl;
  create_window();
}

void
InkApplication::create_window()
{
  std::cout << "InkApplication::create_window" << std::endl;
  _builder = Gtk::Builder::create();

  try
    {
    _builder->add_from_file("ink-application.ui");
    }
  catch (const Glib::Error& ex)
    {
      std::cerr << "InkApplication: ink_application.xml file not read! " << ex.what() << std::endl;
    }
  Gtk::ApplicationWindow *window;
  _builder->get_widget("window", window);
  Gtk::Notebook *notebook;
  _builder->get_widget("notebook", notebook);
  Gtk::Notebook *notebook2;
  _builder->get_widget("notebook2", notebook2);
  notebook->set_group_name("InkscapeDialogGroup");
  notebook2->set_group_name("InkscapeDialogGroup");
  auto tabs = notebook->get_children();
  for (auto tab:tabs) {
    notebook->set_tab_detachable(*tab, true);
  }
  tabs = notebook2->get_children();
  for (auto tab:tabs) {
    notebook2->set_tab_detachable(*tab, true);
  }
  Gtk::EventBox *dropme;
  _builder->get_widget("dropme", dropme);
  std::vector<Gtk::TargetEntry> target_entries;
  target_entries.push_back( Gtk::TargetEntry("GTK_NOTEBOOK_TAB")) ;
  dropme->signal_drag_data_received().connect(
     sigc::mem_fun(*this, &InkApplication::on_prepend_drag_data));
  dropme->drag_dest_set (target_entries, Gtk::DEST_DEFAULT_MOTION | Gtk::DEST_DEFAULT_DROP, Gdk::ACTION_COPY | Gdk::ACTION_MOVE);
  add_window(*window);
  window->show_all();
}
void
InkApplication::on_prepend_drag_data(const Glib::RefPtr<Gdk::DragContext> context, int x, int y,
                                    const Gtk::SelectionData& selection_data, guint info, guint time)
{
  std::cerr << "InkApplication::on_prepend_drag_data: DEPRICATED" << std::endl;
}

