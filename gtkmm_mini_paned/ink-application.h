#ifndef INK_APPLICATION_H
#define INK_APPLICATION_H

/*
 * The main Inkscape application.
 *
 * Copyright (C) 2018 Tavmjong Bah
 *
 * The contents of this file may be used under the GNU General Public License Version 2 or later.
 *
 */

#include <gtkmm.h>

class InkDialogBase;

class InkApplication : public Gtk::Application
{
 protected:
  InkApplication();

 public:
  static Glib::RefPtr<InkApplication> create();

 protected:
  void on_startup() override;
  void on_activate() override;

 private:
  void create_window();
    void on_prepend_drag_data(const Glib::RefPtr<Gdk::DragContext> context, int x, int y,
                                    const Gtk::SelectionData& selection_data, guint info, guint time);
  Glib::RefPtr<Gtk::Builder> _builder;
};

#endif // INK_APPLICATION_H
