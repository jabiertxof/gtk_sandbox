

#include "test-application.h"

#include <glibmm/i18n.h>
#include <gtk/gtk.h>

#include <iostream>
#include <iomanip>

// ------------------------

TestApplication::TestApplication()
  : Gtk::Application("org.tavmjong.testapplication")
{
  Glib::set_application_name("Test Application");
}

Glib::RefPtr<TestApplication> TestApplication::create()
{
  return Glib::RefPtr<TestApplication>(new TestApplication());
}

void
TestApplication::on_startup()
{
  // Initialize gettext (translations)
  Glib::ustring locale_directory(getenv("PWD"));
  locale_directory += "/po";

  bindtextdomain("test-application", locale_directory.c_str());
  bind_textdomain_codeset("test-application", "UTF-8");
  textdomain("test-application");

  // Test of translations
  std::cout << _("TestApplication start") << std::endl;

  Gtk::Application::on_startup();

  // Add actions
  m_test1 = add_action(              "test1",sigc::mem_fun(*this, &TestApplication::on_test1) );
  m_test2 = add_action_bool(         "test2",sigc::mem_fun(*this, &TestApplication::on_test2), false);
  m_test3 = add_action_radio_integer("test3",sigc::mem_fun(*this, &TestApplication::on_test3), 0);
  m_test4 = add_action_radio_string( "test4",sigc::mem_fun(*this, &TestApplication::on_test4), "Mango");
  Glib::VariantType Double(G_VARIANT_TYPE_DOUBLE);
  m_test5 = add_action_with_parameter("test5",Double,sigc::mem_fun(*this,&TestApplication::on_test5));

  m_reset = add_action(              "reset",sigc::mem_fun(*this, &TestApplication::reset) );
  m_disable = add_action_bool(    "disable", sigc::mem_fun(*this, &TestApplication::disable));
  // Excercise actions (don't need GUI for this).
  activate_action("test1");
  activate_action("test2");
  int x = 2;
  activate_action("test3", Glib::Variant<gint32>::create(x));
  Glib::ustring s("Banana");
  activate_action("test4", Glib::Variant<Glib::ustring>::create(s));
  activate_action("test5", Glib::Variant<double>::create(2.71) );

  bool active = false;
  m_test2->get_state(active);
  std::cout << "test2 state: " << std::boolalpha <<  active << std::endl;

  m_test2->change_state(false);
  m_test2->get_state(active);
  std::cout << "test2 state: " << std::boolalpha <<  active << std::endl;

  activate_action("test2");
  m_test2->get_state(active);
  std::cout << "test2 state: " << std::boolalpha <<  active << std::endl;

  int value = -1;
  m_test3->get_state(value);
  std::cout << "test3 state: " << value << std::endl;

  m_test3->change_state(0); // We haven't setup the radio buttons yet so we don't get loops.

  m_test3->get_state(value);
  std::cout << "test3 state: " << value << std::endl;

  m_test3->change_state(1);
  m_test3->get_state(value);
  std::cout << "test3 state: " << value << std::endl;

  activate_action("test3", Glib::Variant<gint32>::create(2));
  m_test3->get_state(value);
  std::cout << "test3 state: " << value << std::endl;

  Glib::ustring fruit;
  m_test4->get_state(fruit);
  std::cout << "test4 state: " << fruit << std::endl;

  m_test4->set_state(Glib::Variant<Glib::ustring>::create("Mango"));
  m_test4->get_state(fruit);
  std::cout << "test4 state: " << fruit << std::endl;

  activate_action("test4", Glib::Variant<Glib::ustring>::create("Banana"));
  m_test4->get_state(fruit);
  std::cout << "test4 state: " << fruit << std::endl;

  // Keyboard shortcuts ---------------------------------------
  // <primary> is Ctrl on Linux
  // Values set here are overridden by those set by Gio::Menu part of .ui file!
  // (Calling after building menu overrides .ui file but AccelLabel not update.
  set_accel_for_action("app.test1",         "<primary>a");
  set_accel_for_action("app.test2",         "<primary>b");
  set_accel_for_action("app.test3(0)",      "<primary>c");
  set_accel_for_action("app.test3(1)",      "<primary>d");
  set_accel_for_action("app.test3(2)",      "<primary>e");
  set_accel_for_action("app.test4::Mango",  "<primary>f");
  set_accel_for_action("app.test4::Banana", "<primary>g");


  // Construct UI (see Gio::Menu to construct by hand)
  m_refBuilder = Gtk::Builder::create();

  try
    {
      m_refBuilder->add_from_file("test-application.ui");
    }
  catch (const Glib::Error& err)
    {
      std::cerr << "Building UI failed: " << err.what();
    }

  // Add menus to application.
  auto object = m_refBuilder->get_object("menu-bar");
  auto menu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);
  if (!menu) {
    std::cerr << "Menu not found" << std::endl;
  } else {
    // These should use different 'menu' as they have different purposes:
    set_menubar( menu );  // Per window menus
    set_app_menu( menu ); // Application menus: Deprecated!
  }
}

void
TestApplication::on_activate()
{
  std::cout << "TestApplication::on_activate()" << std::endl;
  create_window(); 
}

void TestApplication::on_test1()
{
  std::cout << "on_test1" << std::endl;
}

void TestApplication::on_test2()
{
  bool active = false;
  m_test2->get_state(active);

  active = !active;

  m_test2->change_state(active);

  if (active) {
    std::cout << "on_test2: active" << std::endl;
  } else {
    std::cout << "on_test2: not active" << std::endl;
  }
}

void TestApplication::on_test3( int value )
{
  std::cout << "on_test3: Entrance" << std::endl;
  static bool block = false;
  if (block) return;
  block = true;

  m_test3->change_state( value );

  block = false;

  std::cout << "on_test3: " << value << std::endl;
}

void TestApplication::on_test4( Glib::ustring value )
{
  static bool block = false;
  if (block) return;
  block = true;

  m_test4->change_state( value );

  block = false;

  std::cout << "on_test4: " << value << std::endl;
}

void TestApplication::on_test5( Glib::VariantBase value )
{
  // No state
  Glib::Variant<double> d = Glib::VariantBase::cast_dynamic<Glib::Variant<double> > (value);
  std::cout << "on_test5: " << d.get() << std::endl;
}

void TestApplication::on_test5_wrap()
{
  std::cout << "on_test5_wrap" << std::endl;
  double value = m_adjustment0->get_value();
  // Do some validiation...
  activate_action("test5", Glib::Variant<double>::create(value) );
}


// Reset to default values
void TestApplication::reset()
{
  std::cout << "TestApplication::reset(): Entrance" << std::endl;

  // We can directly change boolean values.
  m_test2->change_state(false);

  // We cannot directly change radio buttons as they trigger changes
  // the other buttons in the group which leads to loops. Activating
  // the action prevents this as the callback blocks additonal
  // callbacks.
  on_test3 (0);
  on_test4 ("Banana");
  //m_test3->change_state(2);
  std::cout << "TestApplication::reset(): Exit" << std::endl;
}


// Reset to default values
void TestApplication::disable()
{
  std::cout << "TestApplication::disable(): Entrance" << std::endl;

  bool active = false;
  m_disable->get_state(active);

  active = !active;

  m_disable->change_state(active);

  if (active) {
    std::cout << "disable: active" << std::endl;
  } else {
    std::cout << "disable: not active" << std::endl;
  }

  m_test1->set_enabled(active);
  m_test3->set_enabled(active);
}


void
TestApplication::create_window()
{
  std::cout << "TestApplication::create_window()" << std::endl;
  auto window = new Gtk::ApplicationWindow();

  add_window( *window );  // Application runs till window is closed.
  window->set_title("Test Application");

  window->signal_hide().connect(
    sigc::bind<Gtk::Window*>(sigc::mem_fun(*this, &TestApplication::on_window_hide), window)
  );

  // Gtk::ApplicationWindow is derived from Gtk::Bin so it can contain only one child.
  auto box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
  window->add(*box);

  Gtk::MenuBar* menubar = nullptr;
  m_refBuilder->get_widget("menu-bar2", menubar);
  if (!menubar) {
    std::cerr << "Menubar2 not found" << std::endl;
    return;
  }

  box->add(*menubar);

  // Actions in Tool Bar ------------------------------------

  Gtk::Toolbar* toolbar = nullptr;
  m_refBuilder->get_widget("toolbar", toolbar);
  if (!toolbar) {
    std::cerr << "Toolbar not found" << std::endl;
    return;
  }

  box->add(*toolbar);

  // A Label Tool Item
  Gtk::ToolItem* item0 = nullptr;
  m_refBuilder->get_widget("tool_item_0", item0);
  if (!item0) {
    std::cerr << "Item item0 not found" << std::endl;
  } else {

    // Add widget
    auto label0 = Gtk::manage(new Gtk::Label("MyLabel: A very, very, very, very, very, very, very, long label to push the buttons off the end of the toolbar.") );
    item0->add(*label0);
  }

  // Another Label Tool Item, with overflow Menu
  Gtk::ToolItem* item1 = nullptr;
  m_refBuilder->get_widget("tool_item_1", item1);
  if (!item1) {
    std::cerr << "Item item1 not found" << std::endl;
  } else {

    // Add widget
    auto label1 = Gtk::manage(new Gtk::Label("MyLabel at end") );
    item1->add(*label1);

    // Add overflow menu item
    auto menu1 = Gtk::manage(new Gtk::MenuItem("MyLabel at end"));
    item1->set_proxy_menu_item("MyLabel", *menu1);
  }


  // A SpinButton
  m_adjustment0 = Gtk::Adjustment::create(50, 0, 100, 1, 10, 0);
  m_adjustment0->signal_value_changed().connect(
    sigc::mem_fun(*this, &TestApplication::on_test5_wrap));

  Gtk::ToolItem* item2 = nullptr;
  m_refBuilder->get_widget("tool_item_2", item2);
  if (!item2) {
    std::cerr << "Item item2 not found" << std::endl;
  } else {

    // Add widget
    auto spin0 = Gtk::manage(new Gtk::SpinButton(m_adjustment0));
    item2->add(*spin0);

    // Add overflow menu item
    auto menu2 = Gtk::manage(new Gtk::MenuItem());
    auto spin1 = Gtk::manage(new Gtk::SpinButton(m_adjustment0));
    menu2->add(*spin1);
    item2->set_proxy_menu_item("MySpin", *menu2);
  }


  // Actions as Buttons --------------------------------------

  auto button = Gtk::manage(new Gtk::Button("Test1"));
  // Gtk::Button not derived from Gtk::Actionable due to ABI issues. Must use Gtk. Fixed in Gtk4.
  // Search stackoverflow for: ABI gtk_actionable_set_action_name
  gtk_actionable_set_action_name( GTK_ACTIONABLE(button->gobj()), "app.test1");
  box->add(*button);


  auto toggle = Gtk::manage(new Gtk::ToggleButton("Test2"));
  gtk_actionable_set_action_name( GTK_ACTIONABLE(toggle->gobj()), "app.test2");
  box->add(*toggle);


  auto radio0 = Gtk::manage(new Gtk::RadioButton("Test3: 0"));
  auto group  = radio0->get_group();
  auto radio1 = Gtk::manage(new Gtk::RadioButton(group, "Test3: 1"));
  auto radio2 = Gtk::manage(new Gtk::RadioButton(group, "Test3: 2"));

  gtk_actionable_set_action_name( GTK_ACTIONABLE(radio0->gobj()), "app.test3");
  gtk_actionable_set_action_name( GTK_ACTIONABLE(radio1->gobj()), "app.test3");
  gtk_actionable_set_action_name( GTK_ACTIONABLE(radio2->gobj()), "app.test3");

  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radio0->gobj()), g_variant_new_int32(0) );
  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radio1->gobj()), g_variant_new_int32(1) );
  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radio2->gobj()), g_variant_new_int32(2) );

  auto box3 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL));
  box3->add(*radio0);
  box3->add(*radio1);
  box3->add(*radio2);
  box->add(*box3); // This causes the action value to be set to 0.

  auto radios0 = Gtk::manage(new Gtk::RadioButton("Banana"));
  auto groups  = radios0->get_group();
  auto radios1 = Gtk::manage(new Gtk::RadioButton(groups, "Mango"));

  gtk_actionable_set_action_name( GTK_ACTIONABLE(radios0->gobj()), "app.test4");
  gtk_actionable_set_action_name( GTK_ACTIONABLE(radios1->gobj()), "app.test4");

  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radios0->gobj()), g_variant_new_string("Banana") );
  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radios1->gobj()), g_variant_new_string("Mango") );

  auto box4 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL));
  box4->add(*radios0);
  box4->add(*radios1);
  box->add(*box4);

  auto button2 = Gtk::manage(new Gtk::Button("Reset"));
  gtk_actionable_set_action_name( GTK_ACTIONABLE(button2->gobj()), "app.reset");
  box->add(*button2);

  auto button3 = Gtk::manage(new Gtk::Button("Disable"));
  gtk_actionable_set_action_name( GTK_ACTIONABLE(button3->gobj()), "app.disable");
  box->add(*button3);

  // Menu via GtkAction -------------------------------------------
  /*
  m_beep = Beep::create("beep", "format-justify-center");
  Glib::RefPtr< Glib::Object > actgrp = m_refBuilder->get_object("actiongroup");
  //  Gtk::ActionGroup* actgrp = m_refBuilder->get_object("actiongroup");
  if (!actgrp) {
    std::cerr << "Item actiongroup not found" << std::endl;
  } else {
    Glib::RefPtr< Gtk::ActionGroup >::cast_static(actgrp)->add(m_beep);
    std::cerr << "Added 'beep' to group" << std::endl;
  }

  Gtk::MenuBar* bar0 = nullptr;
  m_refBuilder->get_widget("menubar0", bar0);
  if (!bar0) {
    std::cerr << "Item bar0 not found" << std::endl;
  } else {
    box->add(*bar0);
  }
  
  // Toolbar via GtkAction
  Gtk::Toolbar* tool0 = nullptr;
  m_refBuilder->get_widget("toolbar0", tool0);
  if (!tool0) {
    std::cerr << "Item tool0 not found" << std::endl;
  } else {
    box->add(*tool0);
  }

  Gtk::ToolItem* beep0 = nullptr;
  m_refBuilder->get_widget("beep0", beep0);
  if (!beep0) {
    std::cerr << "Item beep not found" << std::endl;
  }

  Glib::RefPtr< Glib::Object > hmm = m_refBuilder->get_object("beep0");
  if (!hmm) {
    std::cerr << "Item hmm not found (Object)" << std::endl;
  }
  */

  // Styling --------------------------------------------------

  auto provider = Gtk::CssProvider::create();
  try {
    provider->load_from_path("test-application.css");
  }
  catch (...) {
  }

  auto const screen = Gdk::Screen::get_default();
  Gtk::StyleContext::add_provider_for_screen (screen, provider, GTK_STYLE_PROVIDER_PRIORITY_USER);

  set_tooltip_recurse(window);

  std::vector<Glib::ustring> action_descriptions = list_action_descriptions();
  for (auto action_description : action_descriptions) {
    std::cout << action_description;
    std::vector<Glib::ustring> accels = get_accels_for_action(action_description);
    for (auto accel : accels) {
      std::cout << "  " << accel;
    }
    std::cout << std::endl;
  }

  window->show_all();

  std::cout << "TestApplication::create_window(): Exit" << std::endl;
}

void
TestApplication::on_window_hide(Gtk::Window* window)
{
  std::cout << "TestApplication::on_window_hide()" << std::endl;
  delete window;
}

void
TestApplication::set_tooltip_recurse(Gtk::Widget* widget)
{
  static int indent = 0;
  indent++;

  Glib::ustring action_description;
  Glib::ustring action_accelerator;

  // Until GTK4 we need to fallback to GTK+
  if (GTK_IS_ACTIONABLE(widget->gobj())) {

    GtkActionable* actionable = GTK_ACTIONABLE(widget->gobj());
    const gchar* name = gtk_actionable_get_action_name (actionable);
    if (name) {
      action_description = name;
      GVariant* variant = gtk_actionable_get_action_target_value(actionable);
      if (variant) {

        if (       g_variant_is_of_type(variant, G_VARIANT_TYPE_INT32)) {
          const int integer = g_variant_get_int32 (variant);
          action_description += "(";
          action_description += std::to_string (integer);
          action_description += ")";

        } else if (g_variant_is_of_type(variant, G_VARIANT_TYPE_STRING)) {
          const gchar* string  = g_variant_get_string (variant, nullptr);
          action_description += "::";
          action_description += string;

        } else {
          std::cerr << "TestApplication::gui_recurse: unhandled variant type: "
                    << g_variant_print (variant, true) << std::endl;
        }

        std::vector<Glib::ustring> accels = get_accels_for_action(action_description);
        if (!accels.empty()) {
          action_accelerator = accels[0];

          Glib::ustring tooltip = widget->get_tooltip_text();
          // To do, strip off any old accelerator info. Format like AccelLabel.
          tooltip += " (" + action_accelerator + ")";
          widget->set_tooltip_text( tooltip );
        }
      }
    }
  }

  // Glib::Value<std::string> string;
  // string.init(G_TYPE_STRING); // Must init!!!!
  // button->get_property_value("action_name", string);
  // std::string action_name(string.get());
  // std::cout << " action_name: "   << action_name << std::endl;

  for (auto i = 0; i < indent; ++i) {
    std::cout << "  ";
  }

  std::cout << std::setw(30) << std::left << widget->get_name();

  for (int i = 8 - indent; i > 0; --i) {
    std::cout << "  ";
  }

  std::cout << "    " << std::setw(20) << std::left << action_description
            << "    " << std::setw(10) << std::left << action_accelerator
            << "    " << widget->get_tooltip_text() << std::endl;;


  auto container = dynamic_cast<Gtk::Container*>(widget);
  if (container) {
    auto children = container->get_children();
    for (auto child: children) {
      set_tooltip_recurse (child);
    }
  }

  indent--;
}
