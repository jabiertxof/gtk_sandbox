
# Usage: ./generate_pot.sh fr

# Extract out translatable strings
xgettext    -L Glade -k_ -kN_ -o test-application.pot ../*.ui
xgettext -j -L C++   -k_ -kN_ -o test-application.pot ../*.cc ../*.h

# Generate localizable file.
msginit --locale=${1} --input=test-application.pot

# Next translate .po file.
